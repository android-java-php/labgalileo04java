/*
Programa que recibe un número entero, y muestra un mensaje: si es
mayor que cero muestra el mensaje que es positivo, si es menor que
cero muestra el mensaje que es negativo, de lo contrario muestra el
mensaje que es cero.
 */
package labgalileo04;

import org.Galileo.controlpanel.LabGl04;

/**
 *
 * @author Josue Daniel Roldan Ochoa
 */
public class LabGalileo04 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
       new LabGl04().setVisible(true);
    }
    
}
